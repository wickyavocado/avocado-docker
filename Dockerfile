FROM php:7.3-fpm-alpine

LABEL maintainer="wicky@avocado-media.nl"

RUN apk add --no-cache \
    && curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer \ 
    && apk add --update nodejs nodejs-npm openssh

RUN apk --update add libzip-dev mysql-client libpng-dev

RUN docker-php-ext-install zip pdo pdo_mysql gd

WORKDIR /var/www